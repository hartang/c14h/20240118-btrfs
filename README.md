# Btrfs und ich

Btrfs ist ein "modernes" Linux-Dateisystem mit Features wie z.B. Copy-on-Write,
transparenter Dateikompression und Subvolumes/Snapshots. Ich erzähle euch, wie
ich Btrfs benutze, welche Features dabei zum Einsatz kommen und mit welchen
Programmen ich diese Features im Alltag verwalte.

Hier befinden sich die "Folien" zu einem Vortrag, den ich am 18.01.2024 im
Rahmen der c14h beim NoName e.V. gehalten habe. Die Aufnahme zum Vortrag ist
hier verlinkt: https://www.noname-ev.de/chaotische_viertelstunde.html#c14h_603


## Ausführen

Die Präsentation wird in einem Container ausgeführt. Dazu benötigt man `podman`
oder `docker` auf dem Rechner. Der folgende Befehl lädt dann den Container
herunter oder baut ihn lokal und startet die Präsentation:

```
$ ./present.sh
```
