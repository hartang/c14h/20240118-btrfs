#!/usr/bin/env bash
set -euo pipefail

# Location of this script
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
# File containing the slide content
SLIDES="slides.md"
# Name of the container
PODMAN_LOCAL_IMAGE="localhost/patat:latest"
PODMAN_REMOTE_IMAGE="registry.gitlab.com/hartang/c14h/20240118-btrfs:0.9.2"

container_runtime=""
if command -v podman &>/dev/null; then
    container_runtime="podman"
elif command -v docker &>/dev/null; then
    container_runtime="docker"
else
    echo "no suitable container runtime detected" 1>&2
    echo "supported runtimes: podman (preferred), docker" 1>&2
    exit 1
fi

declare image=""
while true; do
    if "$container_runtime" image exists "$PODMAN_REMOTE_IMAGE" ||
        "$container_runtime" pull "$PODMAN_REMOTE_IMAGE"; then
        image="$PODMAN_REMOTE_IMAGE"
        break
    fi

    if "$container_runtime" image exists "$PODMAN_LOCAL_IMAGE" ||
        "$container_runtime" build -t "$PODMAN_LOCAL_IMAGE" "$SCRIPTDIR"; then
        image="$PODMAN_LOCAL_IMAGE"
        break
    fi

    echo "failed to obtain a container image to use" 1>&2
    exit 1
done

"$container_runtime" run --rm -it \
    -v "$SCRIPTDIR:$SCRIPTDIR:Z" -w "$SCRIPTDIR" \
    --network none --log-driver none \
    "$image" \
    --watch \
    "$SCRIPTDIR/$SLIDES"
