---
title: Btrfs und ich
author: Andreas Hartmann (@hartan)
patat:
    breadcrumbs: true
    wrap: true
    slideLevel: 2
    margins:
        left: 5
        right: 5
        top: 1
        bottom: 1
...

# Inhalte

1. Btrfs Kurzeinführung
2. Btrfs-Features
    1. Dateikompression
    2. Subvolumes
    3. Snapshots
    4. Deduplication
    5. Weiteres
3. Wie ich Btrfs "verwalte"
    1. Snapshots + Backups
    2. Deduplication


# Btrfs Kurzeinführung

- Teil des Linux-Kernels
- Copy-on-Write Dateisystem
    - Dateien werden erst mal nicht überschrieben
    - Änderungen/neue Inhalte landen in neuen Blöcken auf der Platte
- Auch bekannt als "Linux perpetually unfinished filesystem"
- Standard in Fedora seit Fedora 33


# Btrfs-Features

## Dateikompression [Docs][6]

- Unterstützt verschiedene Algorithmen: zstd, zlib, lzo
- Kleines Benchmarkskript: siehe [Gitlab][1]
- Wenig sinnvoll bei Mediadateien, nützlich bei Texten etc.

. . .

- Beispiel auf meinem Rechner mit `zstd=5` (Beachte *Uncompressed*):

```bash
❯ compsize "$HOME" -x
Processed 1042840 files, 558760 regular extents (853903 refs), 510317 inline.
Type       Perc     Disk Usage   Uncompressed ...
TOTAL       78%       76G          97G        ...
none       100%       68G          68G        ...
zstd        28%      8.2G          28G        ...
```

## Subvolumes [Docs][2]

- Funktioniert wie ein (Btrfs) Dateisystem im Dateisystem
- Vorteil gegenüber Partitionen: Btrfs-Speicher wird geteilt

. . .

- Subvolumes auf meinen Rechnern:
    - `root`: Gemountet als `/`
    - `home`: Gemountet als `/home`
    - `var`: Gemountet als `/var`
    - `@snapshots`: Gemountet als `/home/.snapshots`


## Snapshots

- Dateiänderungen bei CoW-Dateisystemen sind rekursiv
- Snapshots behalten die Referenz auf "Wurzel" eines Subvolumes
- Lassen sich (inkrementell) auf andere (Btrfs-)Dateisysteme sichern

. . .

- Snapshots auf meinen Rechnern:
    - Automatisch alle 15 Minuten von `home`
    - Snapshots von allen Subvolumes zu "besonderen" Anlässen (Major Updates, RGB2R, Congress, ...)

> Vorsicht: Snapshots alleine sind noch keine echten Backups

## Deduplication

- Basiert auf refs/reflinks, Pointer auf Dateiinhalte [Docs][3]
- In Btrfs Out-of-band (Erst wird geschrieben, dann dedupliziert)
- Auf meinen Rechnern läuft Deduplication permanent im Hintergrund

. . .

- Beispiel von meinem Server (Beachte *Referenced*):
```
❯ sudo compsize data/backup
Processed 145461435 files, 7164653 regular extents (257288810 refs), 80710445 inline.
Type       Perc     Disk Usage   Uncompressed Referenced
TOTAL       34%      214G         619G          16T
...
```

## Weiteres

- [RAID][4]: Unterstützt mehrere Devices/Partitionen
    - Bei mir: Laptop/Desktop laufen mit RAID 1
- [Checksums][5] und [Auto-repair][7] für Daten und Metadaten
    - Eigentlich schon "erledigt" mit RAID 1
    - Auf Rechnern ohne RAID speichere ich Metadaten doppelt
- Online Resize möglich
    - Sehr praktisch bei Installationen


# Wie ich Btrfs "verwalte"

## Snapshots + Backups

- Erledige ich mit [btrbk][8]
- Verpackt in [container][9]
- Gesteuert von einem [systemd service][10] + [timer][11]

. . .

- Mit folgender config:

```toml
snapshot_preserve_min  96h
snapshot_preserve      96h 14d *w

volume /hostfs/
    snapshot_dir @snapshots
    subvolume home
```

## Deduplication

- Erledige ich mit [bees][12]
- Verpackt in [container][13]
- Gesteuert von einem [systemd service][14]
- Mit Ressourcenkontrolle durch [systemd slice][15]

> Ohne Limits wären CPU und Festplatte regelmäßig voll beschäftigt...


# EOF



[1]: https://gitlab.com/hartang/btrfs-compression-test
[2]: https://btrfs.readthedocs.io/en/latest/Subvolumes.html
[3]: https://btrfs.readthedocs.io/en/latest/Reflink.html
[4]: https://btrfs.readthedocs.io/en/latest/Volume-management.html
[5]: https://btrfs.readthedocs.io/en/latest/Checksumming.html
[6]: https://btrfs.readthedocs.io/en/latest/Compression.html
[7]: https://btrfs.readthedocs.io/en/latest/Auto-repair.html
[8]: https://github.com/digint/btrbk
[9]: https://gitlab.com/c8160/btrbk
[10]: https://gitlab.com/hartang/btrfs/btrfs-utils/-/blob/main/systemd/btrbk@.service
[11]: https://gitlab.com/hartang/btrfs/btrfs-utils/-/blob/main/systemd/btrbk@.timer
[12]: https://github.com/Zygo/bees
[13]: https://gitlab.com/c8160/bees
[14]: https://gitlab.com/hartang/btrfs/btrfs-utils/-/blob/main/systemd/bees@.service
[15]: https://gitlab.com/hartang/btrfs/btrfs-utils/-/blob/main/systemd/machine-bees.slice
